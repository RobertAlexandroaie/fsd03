/**
 * 
 */
package fsd.lab.main;

import java.util.logging.Logger;

import fsd.lab.model.LeaderFoundException;
import fsd.lab.model.message.HirschbergSinclairMessage;
import fsd.lab.model.message.MessageType;
import fsd.lab.model.ring.EmptyRingException;
import fsd.lab.model.ring.MessageSourceDirection;
import fsd.lab.model.ring.RingElement;
import fsd.lab.model.ring.RingTopology;
import mpi.MPI;
import mpi.Request;
import mpi.Status;

/**
 * @author Robert
 *
 */
public class Main {
	public static Logger LOGGER = Logger.getLogger(Main.class.getCanonicalName());

	/**
	 * 
	 */
	private static final int DEFAULT_TAG = 0;
	/**
	 * 
	 */
	private static final int MAX_MESSAGE_SIZE = 249;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		RingTopology<Long> ringTopology = new RingTopology<>();
		ringTopology.addRingElementWithValue(new Long(5));
		ringTopology.addRingElementWithValue(new Long(21));
		ringTopology.addRingElementWithValue(new Long(5));
		ringTopology.addRingElementWithValue(new Long(93));
		ringTopology.addRingElementWithValue(new Long(42));

		MPI.Init(args);

		int rank = MPI.COMM_WORLD.Rank();
		// int size = MPI.COMM_WORLD.Size();

		try {
			while (true) {
				RingElement<Long> currentElement = ringTopology.getElement(rank);
				RingElement<Long> leftNeighbour = ringTopology.getLeftNeighbour(currentElement);
				RingElement<Long> rightNeighbour = ringTopology.getRightNeighbour(currentElement);
				int leftId = leftNeighbour.getId();
				int rightId = rightNeighbour.getId();
				boolean receivedMessage = handleReceivedMessages(rank, leftId, rightId, ringTopology);
				if (!receivedMessage && currentElement.isAsleep()) {
					currentElement.setAsleep(false);
					int phase = 0;
					int distance = 1;
					HirschbergSinclairMessage message = new HirschbergSinclairMessage(MessageType.PROBE, rank, phase,
							distance);
					byte[] messageBytes = message.serialise();

					MPI.COMM_WORLD.Isend(messageBytes, 0, MAX_MESSAGE_SIZE, MPI.BYTE, leftId, DEFAULT_TAG);
					MPI.COMM_WORLD.Isend(messageBytes, 0, MAX_MESSAGE_SIZE, MPI.BYTE, rightId, DEFAULT_TAG);
				}
			}
		} catch (EmptyRingException e) {
			e.printStackTrace();
		} catch (LeaderFoundException e) {
			LOGGER.info(e.getMessage() + ": " + e.getLeaderId());
		}
		MPI.Finalize();
	}

	/**
	 * @param currentProcess
	 * @param leftId
	 * @param rightId
	 * @param ringTopology
	 * @throws LeaderFoundException
	 */
	private static boolean handleReceivedMessages(int currentProcess, int leftId, int rightId,
			RingTopology<?> ringTopology) throws LeaderFoundException {
		boolean anyMessageReceived = false;
		anyMessageReceived = handleMessage(rightId, leftId, rightId, leftId, ringTopology);
		anyMessageReceived = handleMessage(currentProcess, leftId, rightId, rightId, ringTopology);
		return anyMessageReceived;
	}

	/**
	 * @param currentProcess
	 * @param leftId
	 * @param rightId
	 * @param sourceId
	 * @param ringTopology
	 * @return
	 * @throws LeaderFoundException
	 */
	private static boolean handleMessage(int currentProcess, int leftId, int rightId, int sourceId,
			RingTopology<?> ringTopology) throws LeaderFoundException {
		boolean anyMessageReceived = false;
		byte[] messageBytes = new byte[MAX_MESSAGE_SIZE];
		Request request = MPI.COMM_WORLD.Irecv(messageBytes, 0, MAX_MESSAGE_SIZE, MPI.BYTE, sourceId, DEFAULT_TAG);
		Status status = request.Test();
		if (status != null) {
			anyMessageReceived = true;
			request.Wait();
			HirschbergSinclairMessage message = HirschbergSinclairMessage.getInstance(messageBytes);
			MessageSourceDirection sourceDirection = rightId == sourceId ? MessageSourceDirection.FROM_RIGHT
					: MessageSourceDirection.FROM_LEFT;
			handleMessage(currentProcess, message, ringTopology, leftId, rightId, sourceDirection);
		}
		return anyMessageReceived;
	}

	/**
	 * @param currentProcess
	 * @param message
	 * @param ringTopology
	 * @param rightId
	 * @param leftId
	 * @param sourceDirection
	 * @throws LeaderFoundException
	 */
	private static void handleMessage(int currentProcess, HirschbergSinclairMessage message,
			RingTopology<?> ringTopology, int leftId, int rightId, MessageSourceDirection sourceDirection)
					throws LeaderFoundException {
		MessageType messageType = message.getMessageType();
		switch (messageType) {
		case PROBE:
			handleProbeMessage(currentProcess, message, sourceDirection, ringTopology, leftId, rightId);
			break;
		case REPLY:
			handleReplyMessage(currentProcess, message, sourceDirection, ringTopology, leftId, rightId);
			break;
		case LEADER:
			handleLeaderMessage(currentProcess, message, sourceDirection, ringTopology, leftId);
			break;
		}
	}

	/**
	 * @param currentProcess
	 * @param message
	 * @param sourceDirection
	 * @param ringTopology
	 * @param rightId
	 * @param leftId
	 */
	private static void handleProbeMessage(int currentProcess, HirschbergSinclairMessage message,
			MessageSourceDirection sourceDirection, RingTopology<?> ringTopology, int leftId, int rightId) {
		int senderId = message.getSenderId();
		int distance = message.getDistance();
		int phase = message.getPhase();
		int frontier = 1 << phase;
		if (senderId == currentProcess) {
			HirschbergSinclairMessage leaderMessage = new HirschbergSinclairMessage(MessageType.LEADER, senderId);
			byte[] messageBytes = leaderMessage.serialise();
			MPI.COMM_WORLD.Isend(messageBytes, 0, MAX_MESSAGE_SIZE, MPI.BYTE, leftId, DEFAULT_TAG);
		} else if (senderId > currentProcess) {
			if (distance < frontier) {
				HirschbergSinclairMessage newProbeMessage = new HirschbergSinclairMessage(message.getMessageType(),
						senderId, phase, distance + 1);
				byte[] messageBytes = newProbeMessage.serialise();
				int destination;
				if (sourceDirection.equals(MessageSourceDirection.FROM_LEFT)) {
					destination = rightId;
				} else {
					destination = leftId;
				}
				MPI.COMM_WORLD.Isend(messageBytes, 0, MAX_MESSAGE_SIZE, MPI.BYTE, destination, DEFAULT_TAG);
			} else {
				HirschbergSinclairMessage replyMessage = new HirschbergSinclairMessage(MessageType.REPLY, senderId,
						phase);
				byte[] messageBytes = replyMessage.serialise();
				int destination;
				if (sourceDirection.equals(MessageSourceDirection.FROM_RIGHT)) {
					destination = rightId;
				} else {
					destination = leftId;
				}
				MPI.COMM_WORLD.Isend(messageBytes, 0, MAX_MESSAGE_SIZE, MPI.BYTE, destination, DEFAULT_TAG);
			}
		}
	}

	/**
	 * @param currentProcess
	 * @param message
	 * @param sourceDirection
	 * @param ringTopology
	 * @param leftId
	 * @param rightId
	 */
	@SuppressWarnings("unchecked")
	private static void handleReplyMessage(int currentProcess, HirschbergSinclairMessage message,
			MessageSourceDirection sourceDirection, RingTopology<?> ringTopology, int leftId, int rightId) {
		int senderId = message.getSenderId();
		int phase = message.getPhase();
		if (senderId != currentProcess) {
			byte[] messageBytes = message.serialise();
			int destination;
			if (sourceDirection.equals(MessageSourceDirection.FROM_LEFT)) {
				destination = rightId;
			} else {
				destination = leftId;
			}
			MPI.COMM_WORLD.Isend(messageBytes, 0, MAX_MESSAGE_SIZE, MPI.BYTE, destination, DEFAULT_TAG);
		} else if (senderId == currentProcess) {
			try {
				RingElement<Long> currentRingElement = (RingElement<Long>) ringTopology.getElement(currentProcess);
				int repliesReceived = currentRingElement.getRepliesReceived();
				currentRingElement.setRepliesReceived(repliesReceived++);
				if (repliesReceived > 1) {
					HirschbergSinclairMessage nextPhaseProbeMessage = new HirschbergSinclairMessage(MessageType.PROBE,
							senderId, phase + 1, 1);
					byte[] messageBytes = nextPhaseProbeMessage.serialise();
					MPI.COMM_WORLD.Isend(messageBytes, 0, MAX_MESSAGE_SIZE, MPI.BYTE, leftId, DEFAULT_TAG);
					MPI.COMM_WORLD.Isend(messageBytes, 0, MAX_MESSAGE_SIZE, MPI.BYTE, rightId, DEFAULT_TAG);
				}
			} catch (EmptyRingException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * @param currentProcess
	 * @param message
	 * @param sourceDirection
	 * @param ringTopology
	 * @param leftId
	 * @throws LeaderFoundException
	 */
	private static void handleLeaderMessage(int currentProcess, HirschbergSinclairMessage message,
			MessageSourceDirection sourceDirection, RingTopology<?> ringTopology, int leftId)
					throws LeaderFoundException {
		int senderId = message.getSenderId();
		ringTopology.setLeader(senderId);
		if (senderId != currentProcess) {
			HirschbergSinclairMessage newLeaderMessage = new HirschbergSinclairMessage(MessageType.LEADER, senderId);
			byte[] messageBytes = newLeaderMessage.serialise();
			MPI.COMM_WORLD.Isend(messageBytes, 0, MAX_MESSAGE_SIZE, MPI.BYTE, leftId, DEFAULT_TAG);
		}
		throw new LeaderFoundException(senderId);
	}
}
