/**
 * 
 */
package fsd.lab.model;

/**
 * @author Robert
 *
 */
public class LeaderFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String DEFAULT_MESSAGE = "New leader has been found";
	private int leaderId;

	public LeaderFoundException(int leaderId) {
		this(leaderId, DEFAULT_MESSAGE);
	}

	public LeaderFoundException(int leaderId, String message) {
		super(message);
		this.leaderId = leaderId;
	}

	/**
	 * @return the leaderId
	 */
	public synchronized int getLeaderId() {
		return leaderId;
	}

	/**
	 * @param leaderId
	 *            the leaderId to set
	 */
	public synchronized void setLeaderId(int leaderId) {
		this.leaderId = leaderId;
	}

}
