/**
 * 
 */
package fsd.lab.model;

/**
 * @author Robert
 *
 */
public interface MPISerialisable<T> {
	byte[] serialise();
	T deserialise(byte[] bytes);
}
